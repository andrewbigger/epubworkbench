﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using epubworkbench.models;
using epubworkbench.helpers;
using System.Diagnostics;
using System.IO;

namespace test.models
{
    [TestClass]
    public class DocumentCollectionTest
    {

        static String tempDir = System.IO.Path.GetTempPath() + "test_dir";
        static String tempFile = tempDir + "\\test.txt";

        private DocumentCollection NonExistentDir = new DocumentCollection("path/to/folder");
        private DocumentCollection Dir;

        private void setUp()
        {
            IO.WriteDir(tempDir);
            IO.WriteText(tempFile, "some-data");
            Dir = new DocumentCollection(tempDir);
        }

        private void tearDown()
        {
            File.Delete(tempFile);
            Directory.Delete(tempDir);
        }

        #region Test: Exists()

        [TestMethod]
        public void ExistsFalseOnDirectoryWhichDoesNotExist()
        {
            Assert.IsFalse(NonExistentDir.Exists);
        }

        [TestMethod]
        public void ExistsTrueOnDirecotyWhichDoesExist()
        {
            setUp();
            Assert.IsTrue(Dir.Exists);
            tearDown();

        }

        #endregion

        #region Test: Name()

        [TestMethod]
        public void ReturnsNameEvenWhenDirectoryDoesNotExist()
        {
            Assert.IsTrue(NonExistentDir.Name == "folder");
        }

        [TestMethod]
        public void ReturnsNameWhenDirectoryDoesExist()
        {
            setUp();
            Assert.IsTrue(Dir.Name == "test_dir");
            tearDown();
        }

        #endregion

        #region Test: Contents()

        [TestMethod]
        public void ReturnsEmptyCollectionForNonExistentDirectory()
        {
            Assert.IsTrue(NonExistentDir.Contents.Count == 0);
        }

        [TestMethod]
        public void ReturnsCollectionWithTestDocument()
        {
            setUp();
            Assert.IsTrue(Dir.Contents.Count == 1);
            tearDown();
        }

        #endregion

        #region Test: LoadCollection()

        [TestMethod]
        public void LoadCollectionDoesntCrash()
        {
            try
            {
                NonExistentDir.LoadCollection();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                Assert.Fail();
            }

        }

        [TestMethod]
        public void ShouldLoadCollectionRecursively()
        {
            setUp();
            String anotherDir = tempDir + "\\another_dir";
            String anotherFile = anotherDir + "\\another_file.txt";
            IO.WriteDir(anotherDir);
            IO.WriteText(anotherFile, "some-data");

            Dir.LoadCollection();

            Assert.IsTrue(Dir.Contents.Count == 2);

            DocumentCollection sub = (DocumentCollection) Dir.Contents[1];
            Assert.IsTrue(sub.Contents.Count == 1);

            File.Delete(anotherFile);
            Directory.Delete(anotherDir);
            tearDown();

        }

        #endregion

        #region Test: Delete()

        [TestMethod]
        public void ShouldDeleteCollectionRecursively()
        {
            setUp();
            String anotherDir = tempDir + "\\another_dir";
            String anotherFile = anotherDir + "\\another_file.txt";
            IO.WriteDir(anotherDir);
            IO.WriteText(anotherFile, "some-data");

            Dir.LoadCollection();

            Assert.IsTrue(Dir.Contents.Count == 2);

            DocumentCollection sub = (DocumentCollection)Dir.Contents[1];
            Assert.IsTrue(sub.Contents.Count == 1);
            Dir.Delete();

            Assert.IsFalse(sub.Exists);
            Assert.IsFalse(Dir.Exists);

        }

        #endregion

    }
}
