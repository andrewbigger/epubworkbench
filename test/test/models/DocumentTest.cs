﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using epubworkbench.models;
using epubworkbench.helpers;
using System.Diagnostics;
using System.IO;

namespace test.models
{
    [TestClass]
    public class DocumentTest
    {
        static String tempPath = System.IO.Path.GetTempPath() + "\\" + "test.txt";

        private Document NonExistentDoc = new Document("path/to/file");
        private Document Doc = new Document(tempPath);

        #region Test: Exist()

        [TestMethod]
        public void ExistsFalseOnDocumentWhichDoesNotExist()
        {
            Assert.IsFalse(NonExistentDoc.Exists);
        }

        [TestMethod]
        public void ExistsTrueOnDocumentWhichDoesExist()
        {
            IO.WriteText(tempPath, "some-data");
            Assert.IsTrue(Doc.Exists);
            File.Delete(tempPath);
        }

        #endregion

        #region Test: Name()

        [TestMethod]
        public void ReturnsNameEvenWhenDocumentDoesNotExist()
        {
            Assert.IsTrue(NonExistentDoc.Name == "file");
        }

        [TestMethod]
        public void ReturnsNameWhenDocumentDoesExist()
        {
            Assert.IsTrue(Doc.Name == "test.txt");
        }

        #endregion

        #region Test: Type()

        [TestMethod]
        public void ReturnsNothingForDocumentThatDoesNotExist()
        {
            Assert.IsTrue(NonExistentDoc.Type == "");
        }

        [TestMethod]
        public void ReturnsDocumentContentsWhenDocumentDoesExist()
        {
            IO.WriteText(tempPath, "some-data");
            Assert.IsTrue(Doc.Type == ".txt");
            File.Delete(tempPath);
        }

        #endregion

        #region Test: Read()

        [TestMethod]
        public void ReturnsNothingOnDocumentWhichDoesNotExist()
        {
            Assert.IsTrue(NonExistentDoc.Read == "");
        }

        [TestMethod]
        public void ReturnsContentsOfDocumentWhichDoesExist()
        {
            IO.WriteText(tempPath, "some-data");
            Assert.IsTrue(Doc.Read == "some-data");
            File.Delete(tempPath);
        }

        #endregion

        #region Test: Write()

        [TestMethod]
        public void TryWriteToDocumentWhichDoesNotExist()
        {
            try
            {
                NonExistentDoc.Write("some-data");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Assert.IsFalse(true == false);
            }
        }

        [TestMethod]
        public void TryWriteToDocumentWhichDoesExist()
        {
            Doc.Write("some-data");
            Assert.IsTrue(Doc.Read == "some-data");
            File.Delete(tempPath);
        }

        #endregion

        #region Test: Delete()
        
        [TestMethod]
        public void DeleteADocument()
        {
            Doc.Write("some-data");
            Assert.IsTrue(Doc.Exists);
            Doc.Delete();
            Assert.IsFalse(Doc.Exists);
        }

        #endregion
    }
}
