﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using epubworkbench.models;
using epubworkbench.helpers;
using epubworkbench.views;

namespace test.helpers
{
    [TestClass]
    public class ApplicationHelperTest
    {

        #region Test: CanOpen()

        [TestMethod]
        public void CanOpenTrue()
        {
            Document supportedDoc = new Document("some/path.opf");
            Assert.IsTrue(ApplicationHelper.CanOpen(supportedDoc));
        }

        [TestMethod]
        public void CanOpenFalse()
        {
            Document unSupportedDoc = new Document("some/path.ext");
            Assert.IsFalse(ApplicationHelper.CanOpen(unSupportedDoc));
        }


        [TestMethod]
        public void CanOpenNoType()
        {
            Document noTypeDoc = new Document("some/mimetype");
            Assert.IsFalse(ApplicationHelper.CanOpen(noTypeDoc));
        }

        #endregion

        #region Test: CanPreview()

        [TestMethod]
        public void CanPreviewTrue()
        {
            Document supportedDoc = new Document("some/path.html");
            Assert.IsTrue(ApplicationHelper.CanOpen(supportedDoc));
        }

        [TestMethod]
        public void CanPreviewFalse()
        {
            Document unSupportedDoc = new Document("some/path.ttf");
            Assert.IsFalse(ApplicationHelper.CanOpen(unSupportedDoc));
        }


        [TestMethod]
        public void CanPreviewNoType()
        {
            Document noTypeDoc = new Document("some/mimetype");
            Assert.IsFalse(ApplicationHelper.CanOpen(noTypeDoc));
        }

        #endregion
       
    }
}
