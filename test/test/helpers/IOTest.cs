﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using epubworkbench.helpers;
using test;
using System.IO;
using System.Collections.Generic;

namespace test.helpers
{
    [TestClass]
    public class IOTest
    {
        #region Test: WriteText()

        [TestMethod]
        public void WriteFile()
        {
            // writes text out
            String sample = "Some text for export";
            String tempPath = System.IO.Path.GetTempPath() + "\\" + "test.txt";
            IO.WriteText(tempPath, sample);

            Assert.IsTrue(File.Exists(tempPath));
            Assert.IsTrue(IO.ReadText(tempPath, "") == sample);

            File.Delete(tempPath);

            Assert.IsFalse(File.Exists(tempPath));
        }

        #endregion

        #region Test: ReadText()

        [TestMethod]
        public void ReadFileToList()
        {
            // imports text file into list of strings
            String sample = "Some text for import";
            String tempPath = System.IO.Path.GetTempPath() + "\\" + "test.txt";
            IO.WriteText(tempPath, sample);
            
            List<String> result = IO.ReadText(tempPath);

            Assert.IsTrue(result.Count == 1);
            Assert.IsTrue(result.Contains(sample));

            File.Delete(tempPath);

            Assert.IsFalse(File.Exists(tempPath));

        }

        [TestMethod]
        public void ReadFileToString()
        {
            // reads text file into string (with chosen delimiter)"
            String sample = "Some text for import\n Some other stuff";
            String tempPath = System.IO.Path.GetTempPath() + "\\" + "test.txt";
            IO.WriteText(tempPath, sample);

            String result = IO.ReadText(tempPath, "|");

            Assert.IsTrue(result.Contains("Some text for import| Some other stuff"));

            File.Delete(tempPath);

            Assert.IsFalse(File.Exists(tempPath));
        }

        #endregion

        #region Test: WriteDir();

        [TestMethod]
        public void TryCreateDirectoryToPlaceThatDoesntExist()
        {
            IO.WriteDir("non/exist/path");
            try
            {
                Directory.Exists("non/exist/path");
                Assert.Fail();
            }
            catch (Exception)
            {
                Assert.IsFalse(false);
            }
        }

        [TestMethod]
        public void TryCreateDirectoryToPlaceWhichDoesExist()
        {
            String tempPath = System.IO.Path.GetTempPath() + "\\" + "test";
            IO.WriteDir(tempPath);

            Assert.IsTrue(Directory.Exists(tempPath));

            Directory.Delete(tempPath);

            Assert.IsFalse(Directory.Exists(tempPath));
        }

    }

        #endregion

}
