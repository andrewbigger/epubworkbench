﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace epubworkbench.helpers
{
    public class IO
    {

        public static void WriteDir(String path)
        {
            try
            {
                Directory.CreateDirectory(path);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
            }
        }

        public static void DeleteDir(String path)
        {
            try
            {
                Directory.Delete(path);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
            }
        }

        public static void WriteText(String path, String data)
        {
            try
            {
                StreamWriter sw = new StreamWriter(path);
                sw.Write(data);
                sw.Flush();
                sw.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }

        }

        public static List<String> ReadText(String path)
        {

            String line;
            List<String> content = new List<String>();

            try
            {
                StreamReader sr = new StreamReader(path);
                line = sr.ReadLine();
                while (line != null)
                {
                    content.Add(line);
                    line = sr.ReadLine(); //read the next line
                }

                sr.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }

            return content;

        }

        public static String ReadText(String path, String sep)
        {
            return String.Join(sep, ReadText(path));
        }

        public static void DeleteText(String path)
        {
            try
            {
                File.Delete(path);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
            }
        }

        public static void CopyFile(Stream source, Stream target)
        {
            byte[] buffer = new byte[0x10000];
            int bytes;
            try
            {
                while ((bytes = source.Read(buffer, 0, buffer.Length)) > 0)
                {
                    target.Write(buffer, 0, bytes);
                }
            }
            finally
            {
                target.Close();
            }
        }

    }
}
