﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using epubworkbench.models;
using epubworkbench.views;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace epubworkbench.helpers
{
    public class ApplicationHelper
    {
        #region Application Properties

        public static String SpecialFolder()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        }

        public static Boolean CanOpen(Document doc)
        {
            return SupportedFiles().Contains(doc.Type);
        }

        public static Boolean CanPreview(Document doc)
        {
            return SupportedPreviews().Contains(doc.Type);
        }

        #endregion

        #region Application Metadata

        private static Collection<String> SupportedPreviews()
        {
            Collection<String> supportedFiles = new Collection<String>();
            supportedFiles.Add(".html");
            supportedFiles.Add(".xhtml");
            supportedFiles.Add(".plist");
            supportedFiles.Add(".opf");
            supportedFiles.Add(".ncx");
            supportedFiles.Add(".xml");
            supportedFiles.Add(".jpg");
            supportedFiles.Add(".png");
            supportedFiles.Add(".gif");
            return supportedFiles;
        }

        private static Collection<String> SupportedFiles()
        {
            Collection<String> supportedFiles = new Collection<String>();
            supportedFiles.Add(".html");
            supportedFiles.Add(".xhtml");
            supportedFiles.Add(".plist");
            supportedFiles.Add(".opf");
            supportedFiles.Add(".ncx");
            supportedFiles.Add(".xml");
            supportedFiles.Add(".css");
            return supportedFiles;
        }

        #endregion

        #region Actions

        public static void About()
        {
            frmAbout about = new frmAbout();
            about.ShowDialog();
        }

        public static void Documentation()
        {
            ApplicationHelper.OpenSystemDefault(Properties.Settings.Default.DocumentaitonURL);
        }

        public static void Settings()
        {
            frmSettings settings = new frmSettings();
            settings.ShowDialog();
        }

        public static void OpenFile(Document doc, frmHtml module)
        {
            if (Properties.Settings.Default.HTMLEditorEnabled == true)
            {
                module.Show();
            }
            else
            {
                OpenSystemDefault(doc);
            }
        }

        public static void OpenFile(Document doc, frmText module)
        {
            if (Properties.Settings.Default.TextWindowEnabled == true)
            {
                module.Show(); 
            }
            else 
            { 
                OpenSystemDefault(doc);
            }
        }

        public static void OpenFile(Document doc)
        {

            switch (doc.Type)
            {
                case ".html":
                case ".xhtml":
                    XhtmlDocument xhtmlDoc = new XhtmlDocument(doc);
                    OpenFile(xhtmlDoc, new frmHtml(xhtmlDoc, true));
                    break;
                case ".plist":
                case ".opf":
                case ".ncx":
                case ".xml":
                case ".css":
                    OpenFile(doc, new frmText(doc));
                    break;
                default:
                    MessageBox.Show(
                    "Epubworkbench cannot open " + doc.Type + " files.",
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                break;

            }

        }

        public static void OpenSystemDefault(Document doc)
        {
            OpenSystemDefault(doc.Location);
        }

        public static void OpenSystemDefault(String doc)
        {
            Process.Start(doc);
        }

        public static void SelectExplorer(Document doc)
        {
            Process.Start("explorer.exe", @"/select, " + doc.Location);
        }

        public static void SelectExplorer(DocumentCollection coll)
        {
            Process.Start("explorer.exe", @"/select, " + coll.Location);
        }

        public static void Exit()
        {
            DialogResult closeDecision = MessageBox.Show("Are you sure you want to exit ePub Workbench? All unsaved changes will be lost.",
                                                         "Exit ePub Workbench",
                                                         MessageBoxButtons.YesNo,
                                                         MessageBoxIcon.Warning);
            if (closeDecision == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        public static void Exit(FormClosingEventArgs e)
        {
            DialogResult closeDecision = MessageBox.Show("Are you sure you want to exit ePub Workbench? All unsaved changes will be lost.",
                                                         "Exit ePub Workbench",
                                                         MessageBoxButtons.YesNo,
                                                         MessageBoxIcon.Warning);
            if (closeDecision == DialogResult.Yes)
            {
                Application.Exit();
            }
            else
            {
                e.Cancel = true;
            }

        }

        #endregion

    }
}
