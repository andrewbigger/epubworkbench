﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.IO;
using epubworkbench.helpers;

namespace epubworkbench.models
{
    public class Document
    {
        protected String _Path;

        public Document()
        {
            
        }

        public Document(String location)
        {
            _Path = location;
        }

        public String Location
        {
            get
            {
                return _Path;
            }
        }

        public String Name
        {
            get
            {
                return Path.GetFileName(_Path);
            }
        }

        public Boolean Exists
        {
            get
            {
                return File.Exists(_Path);
            }
        }

        public String Type
        {
            get
            {
                return Path.GetExtension(_Path);
            }
            
        }

        public String Read
        {
            get
            {
                return IO.ReadText(_Path, "\n");
            }

        }

        public void Write(String data)
        {
            IO.WriteText(_Path, data);
        }

        public void Delete()
        {
            IO.DeleteText(_Path);
        }

    }
}
