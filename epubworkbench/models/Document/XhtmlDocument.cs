﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.IO;
using HtmlAgilityPack;
using epubworkbench.helpers;

namespace epubworkbench.models
{
    public class XhtmlDocument : Document
    {
        private HtmlDocument _XHtml;

        public XhtmlDocument(Document doc)
        {
            _Path = doc.Location;
            _XHtml = new HtmlDocument();
            this.LoadXhtml();
        }

        public void MakeEditable()
        {
            String EditJS = @"<script>
			        document.addEventListener('DOMContentLoaded', function() {
			           document.getElementsByTagName('body')[0].contentEditable=true;
			        }, false);
                </script>
            ";
            HtmlNode EditScript = HtmlNode.CreateNode(EditJS);
            HtmlNode head = _XHtml.DocumentNode.SelectSingleNode("//head");

            head.AppendChild(EditScript);
            this.Save();
        }

        public void Clean()
        {
            this.LoadXhtml();
            HtmlNodeCollection scripts = _XHtml.DocumentNode.SelectNodes("//head/script");
            if (scripts.Count > 1)
            {
                foreach (HtmlNode script in scripts)
                {
                    script.Remove();
                }
                this.Save();
            }

            HtmlNode body = _XHtml.DocumentNode.SelectSingleNode("//body");
            body.Attributes.RemoveAll();
        }

        public void Save()
        {
            _XHtml.Save(this._Path);
        }

        private void LoadXhtml()
        {
            _XHtml.OptionWriteEmptyNodes = true;
            _XHtml.LoadHtml(this.Read);
        }

    }
}
