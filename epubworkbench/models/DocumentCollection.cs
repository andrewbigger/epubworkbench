﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using epubworkbench.helpers;

namespace epubworkbench.models
{
    public class DocumentCollection
    {

        private String _Path;
        private ArrayList _Collection = new ArrayList();

        public DocumentCollection(String location)
        {
            _Path = location;
            if (Exists == true) { LoadCollection(); }
        }

        public String Location
        {
            get
            {
                return _Path;
            }
        }

        public String Name
        {
            get
            {
                DirectoryInfo info = new DirectoryInfo(_Path);
                return info.Name;
            }
        }

        public Boolean Exists
        {
            get
            {
                return Directory.Exists(_Path);
            }
        }

        public ArrayList Contents
        {
            get
            {
                return _Collection;
            }
        }

        public void LoadCollection()
        {
            if (Exists == true)
            {
                _Collection = new ArrayList();
                foreach (String f in Directory.GetFiles(_Path))
                {
                    _Collection.Add(new Document(f));
                }
                foreach (String d in Directory.GetDirectories(_Path))
                {
                    _Collection.Add(new DocumentCollection(d));
                }
            }
        }

        public void Delete()
        {
            LoadCollection();
            foreach (Object o in _Collection)
            {
                try
                {
                    Document d = (Document)o;
                    d.Delete();
                }
                catch (InvalidCastException)
                {
                    //ignore
                }

                try
                {
                    DocumentCollection dc = (DocumentCollection)o;
                    dc.Delete();
                }
                catch (InvalidCastException)
                {
                    //ignore
                }
            }

            IO.DeleteDir(_Path);
            
        }


    }
}
