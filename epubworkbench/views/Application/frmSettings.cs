﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace epubworkbench.views
{
    public partial class frmSettings : Form
    {

        #region Constructor and Loader

        public frmSettings()
        {
            InitializeComponent();
        }

        private void frmSettings_Load(object sender, EventArgs e)
        {
            tipAbout.SetToolTip(this.cmbToolbarDisplay, "Sets the display style of items inside toolbars across the application.");
            tipAbout.SetToolTip(this.chkTextEnabled, "Enable or Disable the Workbench text editor.");
            tipAbout.SetToolTip(this.chkTextShowLineNumbers, "Show or hide line numbers in Workbench text editor.");
            tipAbout.SetToolTip(this.chkTextWrapText, "Toggle text wrapping (i.e. horizontal scrolling) in Workbench text editor.");
            tipAbout.SetToolTip(this.chkTextConvertTabsToSpaces, "Choose whether to save space before code as a space or a tab.");
            tipAbout.SetToolTip(this.chkSyntaxHighlighting, "Detect and colour script and markup for easier reading.");
            tipAbout.SetToolTip(this.btnTextFont, "Set font for Workbench text editor.");
            tipAbout.SetToolTip(this.chkSaveOnChange, "Set whether to save whenever text is changed.");
            tipAbout.SetToolTip(this.chkHTMLEnabled, "Enable or Disable the Workbench HTML editor/viewer");
            tipAbout.SetToolTip(this.mceSettings, "Script for configuring HTML (What you see is what you get) WYSIWYG editor.");

            tipAbout.SetToolTip(this.btnOk, "Save settings and close window.");
            tipAbout.SetToolTip(this.btnCancel, "Discard setting changes and close window.");
            tipAbout.SetToolTip(this.btnApply, "Apply settings and leave settings window open.");

        }

        #endregion

        #region Event Handlers

        private void btnOk_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Save();
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Reload();
            this.Close();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        private void chkEnabled_CheckedChanged(object sender, EventArgs e)
        {
            FocusTextSettings(this.chkTextEnabled.Checked);
        }

        private void chkHTMLEnabled_CheckedChanged(object sender, EventArgs e)
        {
        FocusHTMLSettings(this.chkHTMLEnabled.Checked);
        }

        #endregion

        #region Actions

        private void FocusTextSettings(bool enabled)
        {
            this.grpTextDisplay.Enabled = enabled;
            // this.grpTextBehaviour.Enabled = enabled;
        }

        private void FocusHTMLSettings(bool enabled)
        {
            this.grpMCESettings.Enabled = enabled;
        }

        #endregion

    }
}
