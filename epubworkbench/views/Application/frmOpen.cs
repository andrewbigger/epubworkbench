﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using epubworkbench.models;
using epubworkbench.helpers;

namespace epubworkbench.views
{
    public partial class frmOpen : Form
    {
        #region Instance Variables

        private String _target;

        #endregion

        #region Constructor and Loader

        public frmOpen()
        {
            InitializeComponent();
        }

        private void frmOpen_Load(object sender, EventArgs e)
        {
            tipAbout.SetToolTip(this.btnAbout, "About ePub Workbench.");
            tipAbout.SetToolTip(this.btnDocumentation, "Go to Workbench documentation for instructions.");
            tipAbout.SetToolTip(this.btnSettings, "Modify Workbench settings.");
            tipAbout.SetToolTip(this.btnOpen, "Open an expanded epub folder.");
            tipAbout.SetToolTip(this.btnBrowse, "Choose an expanded epub folder.");
        }

        #endregion

        #region Event Handlers

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            Browse();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            Open();
        }

        private void btnAbout_Click(object sender, EventArgs e)
        {
            ApplicationHelper.About();
        }

        private void btnDocumentation_Click(object sender, EventArgs e)
        {
            ApplicationHelper.Documentation();
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            ApplicationHelper.Settings();
        }

        #endregion

        #region Actions

        private void Browse()
        {
            SetPath("", false);
            FolderBrowserDialog directoryChoice = ChooseDir("Please choose an exapanded ePub folder:", false);
            directoryChoice.ShowDialog();

            if (Directory.Exists(directoryChoice.SelectedPath) == true)
            {
                SetPath(directoryChoice.SelectedPath, true);
            }
        }

        private void Open()
        {
            if (Directory.Exists(_target) == true)
            {
                DocumentCollection epub = new DocumentCollection(_target);

                frmBrowser fileBrowser = Browser(epub);
                fileBrowser.Show();
                this.Hide();
            }
        }

        private void SetPath(String path, Boolean canProceed)
        {
            _target = path;
            this.txtPath.Text = path;
            this.btnOpen.Enabled = canProceed;

            if (canProceed == true)
            {
                this.AcceptButton = this.btnOpen;
            }
            else
            {
                this.AcceptButton = this.btnBrowse;
            }
        }

        #endregion

        #region UI Builders

        private frmBrowser Browser(DocumentCollection doc)
        {
            frmBrowser fileBrowser = new frmBrowser(doc);
            fileBrowser.Text = "ePub Workbench : : " + doc.Name;

            return fileBrowser;
        }

        private FolderBrowserDialog ChooseDir(String description, Boolean allowNewFolder)
        {
            FolderBrowserDialog choose = new FolderBrowserDialog();
            choose.ShowNewFolderButton = allowNewFolder;
            choose.Description = description;

            return choose;
        }

        #endregion


    }
}
