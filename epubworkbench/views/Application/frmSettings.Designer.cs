﻿namespace epubworkbench.views
{
    partial class frmSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSettings));
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.tabSettings = new System.Windows.Forms.TabControl();
            this.tabGeneral = new System.Windows.Forms.TabPage();
            this.cmbToolbarDisplay = new System.Windows.Forms.ComboBox();
            this.lblToolbarDisplay = new System.Windows.Forms.Label();
            this.tabTextEditor = new System.Windows.Forms.TabPage();
            this.chkTextEnabled = new System.Windows.Forms.CheckBox();
            this.grpTextBehaviour = new System.Windows.Forms.GroupBox();
            this.chkSaveOnChange = new System.Windows.Forms.CheckBox();
            this.grpTextDisplay = new System.Windows.Forms.GroupBox();
            this.chkSyntaxHighlighting = new System.Windows.Forms.CheckBox();
            this.chkTextConvertTabsToSpaces = new System.Windows.Forms.CheckBox();
            this.chkTextWrapText = new System.Windows.Forms.CheckBox();
            this.chkTextShowLineNumbers = new System.Windows.Forms.CheckBox();
            this.btnTextFont = new System.Windows.Forms.Button();
            this.tabHTMLEditor = new System.Windows.Forms.TabPage();
            this.chkHTMLEnabled = new System.Windows.Forms.CheckBox();
            this.grpMCESettings = new System.Windows.Forms.GroupBox();
            this.mceSettings = new System.Windows.Forms.RichTextBox();
            this.lblWarning = new System.Windows.Forms.Label();
            this.imgSettings = new System.Windows.Forms.ImageList(this.components);
            this.fnt = new System.Windows.Forms.FontDialog();
            this.tipAbout = new System.Windows.Forms.ToolTip(this.components);
            this.tabSettings.SuspendLayout();
            this.tabGeneral.SuspendLayout();
            this.tabTextEditor.SuspendLayout();
            this.grpTextBehaviour.SuspendLayout();
            this.grpTextDisplay.SuspendLayout();
            this.tabHTMLEditor.SuspendLayout();
            this.grpMCESettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(395, 327);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(314, 327);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnApply
            // 
            this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnApply.Location = new System.Drawing.Point(12, 327);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(75, 23);
            this.btnApply.TabIndex = 2;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // tabSettings
            // 
            this.tabSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabSettings.Controls.Add(this.tabGeneral);
            this.tabSettings.Controls.Add(this.tabTextEditor);
            this.tabSettings.Controls.Add(this.tabHTMLEditor);
            this.tabSettings.ImageList = this.imgSettings;
            this.tabSettings.Location = new System.Drawing.Point(12, 12);
            this.tabSettings.Name = "tabSettings";
            this.tabSettings.SelectedIndex = 0;
            this.tabSettings.Size = new System.Drawing.Size(458, 309);
            this.tabSettings.TabIndex = 3;
            // 
            // tabGeneral
            // 
            this.tabGeneral.Controls.Add(this.cmbToolbarDisplay);
            this.tabGeneral.Controls.Add(this.lblToolbarDisplay);
            this.tabGeneral.ImageIndex = 0;
            this.tabGeneral.Location = new System.Drawing.Point(4, 23);
            this.tabGeneral.Name = "tabGeneral";
            this.tabGeneral.Size = new System.Drawing.Size(450, 282);
            this.tabGeneral.TabIndex = 3;
            this.tabGeneral.Text = "General";
            this.tabGeneral.UseVisualStyleBackColor = true;
            // 
            // cmbToolbarDisplay
            // 
            this.cmbToolbarDisplay.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::epubworkbench.Properties.Settings.Default, "ToolbarDisplay", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cmbToolbarDisplay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbToolbarDisplay.FormattingEnabled = true;
            this.cmbToolbarDisplay.Items.AddRange(new object[] {
            "Icon Only",
            "Icon and Text",
            "Text Only"});
            this.cmbToolbarDisplay.Location = new System.Drawing.Point(104, 13);
            this.cmbToolbarDisplay.Name = "cmbToolbarDisplay";
            this.cmbToolbarDisplay.Size = new System.Drawing.Size(327, 21);
            this.cmbToolbarDisplay.TabIndex = 1;
            this.cmbToolbarDisplay.Text = global::epubworkbench.Properties.Settings.Default.ToolbarDisplay;
            // 
            // lblToolbarDisplay
            // 
            this.lblToolbarDisplay.AutoSize = true;
            this.lblToolbarDisplay.Location = new System.Drawing.Point(15, 16);
            this.lblToolbarDisplay.Name = "lblToolbarDisplay";
            this.lblToolbarDisplay.Size = new System.Drawing.Size(83, 13);
            this.lblToolbarDisplay.TabIndex = 0;
            this.lblToolbarDisplay.Text = "Toolbar Display:";
            // 
            // tabTextEditor
            // 
            this.tabTextEditor.Controls.Add(this.chkTextEnabled);
            this.tabTextEditor.Controls.Add(this.grpTextBehaviour);
            this.tabTextEditor.Controls.Add(this.grpTextDisplay);
            this.tabTextEditor.ImageIndex = 1;
            this.tabTextEditor.Location = new System.Drawing.Point(4, 23);
            this.tabTextEditor.Name = "tabTextEditor";
            this.tabTextEditor.Size = new System.Drawing.Size(450, 282);
            this.tabTextEditor.TabIndex = 1;
            this.tabTextEditor.Text = "Text Editor";
            this.tabTextEditor.UseVisualStyleBackColor = true;
            // 
            // chkTextEnabled
            // 
            this.chkTextEnabled.AutoSize = true;
            this.chkTextEnabled.Checked = global::epubworkbench.Properties.Settings.Default.TextWindowEnabled;
            this.chkTextEnabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTextEnabled.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::epubworkbench.Properties.Settings.Default, "TextWindowEnabled", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chkTextEnabled.Location = new System.Drawing.Point(9, 12);
            this.chkTextEnabled.Name = "chkTextEnabled";
            this.chkTextEnabled.Size = new System.Drawing.Size(65, 17);
            this.chkTextEnabled.TabIndex = 3;
            this.chkTextEnabled.Text = "Enabled";
            this.chkTextEnabled.UseVisualStyleBackColor = true;
            this.chkTextEnabled.CheckedChanged += new System.EventHandler(this.chkEnabled_CheckedChanged);
            // 
            // grpTextBehaviour
            // 
            this.grpTextBehaviour.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpTextBehaviour.Controls.Add(this.chkSaveOnChange);
            this.grpTextBehaviour.Enabled = false;
            this.grpTextBehaviour.Location = new System.Drawing.Point(3, 136);
            this.grpTextBehaviour.Name = "grpTextBehaviour";
            this.grpTextBehaviour.Size = new System.Drawing.Size(444, 52);
            this.grpTextBehaviour.TabIndex = 3;
            this.grpTextBehaviour.TabStop = false;
            this.grpTextBehaviour.Text = "Behaviour";
            // 
            // chkSaveOnChange
            // 
            this.chkSaveOnChange.AutoSize = true;
            this.chkSaveOnChange.Checked = global::epubworkbench.Properties.Settings.Default.TextWindowSaveOnChange;
            this.chkSaveOnChange.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::epubworkbench.Properties.Settings.Default, "TextWindowSaveOnChange", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chkSaveOnChange.Location = new System.Drawing.Point(6, 19);
            this.chkSaveOnChange.Name = "chkSaveOnChange";
            this.chkSaveOnChange.Size = new System.Drawing.Size(105, 17);
            this.chkSaveOnChange.TabIndex = 0;
            this.chkSaveOnChange.Text = "Save on change";
            this.chkSaveOnChange.UseVisualStyleBackColor = true;
            // 
            // grpTextDisplay
            // 
            this.grpTextDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpTextDisplay.Controls.Add(this.chkSyntaxHighlighting);
            this.grpTextDisplay.Controls.Add(this.chkTextConvertTabsToSpaces);
            this.grpTextDisplay.Controls.Add(this.chkTextWrapText);
            this.grpTextDisplay.Controls.Add(this.chkTextShowLineNumbers);
            this.grpTextDisplay.Controls.Add(this.btnTextFont);
            this.grpTextDisplay.Location = new System.Drawing.Point(3, 35);
            this.grpTextDisplay.Name = "grpTextDisplay";
            this.grpTextDisplay.Size = new System.Drawing.Size(444, 95);
            this.grpTextDisplay.TabIndex = 2;
            this.grpTextDisplay.TabStop = false;
            this.grpTextDisplay.Text = "Display";
            // 
            // chkSyntaxHighlighting
            // 
            this.chkSyntaxHighlighting.AutoSize = true;
            this.chkSyntaxHighlighting.Checked = global::epubworkbench.Properties.Settings.Default.TextWindowSyntaxHighlighting;
            this.chkSyntaxHighlighting.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::epubworkbench.Properties.Settings.Default, "TextWindowSyntaxHighlighting", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chkSyntaxHighlighting.Enabled = false;
            this.chkSyntaxHighlighting.Location = new System.Drawing.Point(322, 65);
            this.chkSyntaxHighlighting.Name = "chkSyntaxHighlighting";
            this.chkSyntaxHighlighting.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkSyntaxHighlighting.Size = new System.Drawing.Size(116, 17);
            this.chkSyntaxHighlighting.TabIndex = 4;
            this.chkSyntaxHighlighting.Text = "Syntax Highlighting";
            this.chkSyntaxHighlighting.UseVisualStyleBackColor = true;
            // 
            // chkTextConvertTabsToSpaces
            // 
            this.chkTextConvertTabsToSpaces.AutoSize = true;
            this.chkTextConvertTabsToSpaces.Checked = global::epubworkbench.Properties.Settings.Default.TextWindowConvertTabsToSpaces;
            this.chkTextConvertTabsToSpaces.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::epubworkbench.Properties.Settings.Default, "TextWindowConvertTabsToSpaces", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chkTextConvertTabsToSpaces.Enabled = false;
            this.chkTextConvertTabsToSpaces.Location = new System.Drawing.Point(6, 65);
            this.chkTextConvertTabsToSpaces.Name = "chkTextConvertTabsToSpaces";
            this.chkTextConvertTabsToSpaces.Size = new System.Drawing.Size(141, 17);
            this.chkTextConvertTabsToSpaces.TabIndex = 3;
            this.chkTextConvertTabsToSpaces.Text = "Convert Tabs to Spaces";
            this.chkTextConvertTabsToSpaces.UseVisualStyleBackColor = true;
            // 
            // chkTextWrapText
            // 
            this.chkTextWrapText.AutoSize = true;
            this.chkTextWrapText.Checked = global::epubworkbench.Properties.Settings.Default.TextWindowWrapText;
            this.chkTextWrapText.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::epubworkbench.Properties.Settings.Default, "TextWindowWrapText", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chkTextWrapText.Location = new System.Drawing.Point(6, 42);
            this.chkTextWrapText.Name = "chkTextWrapText";
            this.chkTextWrapText.Size = new System.Drawing.Size(76, 17);
            this.chkTextWrapText.TabIndex = 2;
            this.chkTextWrapText.Text = "Wrap Text";
            this.chkTextWrapText.UseVisualStyleBackColor = true;
            // 
            // chkTextShowLineNumbers
            // 
            this.chkTextShowLineNumbers.AutoSize = true;
            this.chkTextShowLineNumbers.Checked = global::epubworkbench.Properties.Settings.Default.TextWindowShowNumbers;
            this.chkTextShowLineNumbers.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::epubworkbench.Properties.Settings.Default, "TextWindowShowNumbers", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chkTextShowLineNumbers.Location = new System.Drawing.Point(6, 19);
            this.chkTextShowLineNumbers.Name = "chkTextShowLineNumbers";
            this.chkTextShowLineNumbers.Size = new System.Drawing.Size(121, 17);
            this.chkTextShowLineNumbers.TabIndex = 0;
            this.chkTextShowLineNumbers.Text = "Show Line Numbers";
            this.chkTextShowLineNumbers.UseVisualStyleBackColor = true;
            // 
            // btnTextFont
            // 
            this.btnTextFont.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTextFont.Enabled = false;
            this.btnTextFont.Location = new System.Drawing.Point(344, 15);
            this.btnTextFont.Name = "btnTextFont";
            this.btnTextFont.Size = new System.Drawing.Size(94, 23);
            this.btnTextFont.TabIndex = 1;
            this.btnTextFont.Text = "Choose Font";
            this.btnTextFont.UseVisualStyleBackColor = true;
            // 
            // tabHTMLEditor
            // 
            this.tabHTMLEditor.Controls.Add(this.chkHTMLEnabled);
            this.tabHTMLEditor.Controls.Add(this.grpMCESettings);
            this.tabHTMLEditor.ImageIndex = 2;
            this.tabHTMLEditor.Location = new System.Drawing.Point(4, 23);
            this.tabHTMLEditor.Name = "tabHTMLEditor";
            this.tabHTMLEditor.Padding = new System.Windows.Forms.Padding(3);
            this.tabHTMLEditor.Size = new System.Drawing.Size(450, 282);
            this.tabHTMLEditor.TabIndex = 2;
            this.tabHTMLEditor.Text = "HTML Editor";
            this.tabHTMLEditor.UseVisualStyleBackColor = true;
            // 
            // chkHTMLEnabled
            // 
            this.chkHTMLEnabled.AutoSize = true;
            this.chkHTMLEnabled.Checked = global::epubworkbench.Properties.Settings.Default.HTMLEditorEnabled;
            this.chkHTMLEnabled.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::epubworkbench.Properties.Settings.Default, "HTMLEditorEnabled", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chkHTMLEnabled.Location = new System.Drawing.Point(9, 12);
            this.chkHTMLEnabled.Name = "chkHTMLEnabled";
            this.chkHTMLEnabled.Size = new System.Drawing.Size(65, 17);
            this.chkHTMLEnabled.TabIndex = 4;
            this.chkHTMLEnabled.Text = "Enabled";
            this.chkHTMLEnabled.UseVisualStyleBackColor = true;
            this.chkHTMLEnabled.CheckedChanged += new System.EventHandler(this.chkHTMLEnabled_CheckedChanged);
            // 
            // grpMCESettings
            // 
            this.grpMCESettings.Controls.Add(this.mceSettings);
            this.grpMCESettings.Controls.Add(this.lblWarning);
            this.grpMCESettings.Enabled = false;
            this.grpMCESettings.Location = new System.Drawing.Point(6, 35);
            this.grpMCESettings.Name = "grpMCESettings";
            this.grpMCESettings.Size = new System.Drawing.Size(438, 241);
            this.grpMCESettings.TabIndex = 0;
            this.grpMCESettings.TabStop = false;
            this.grpMCESettings.Text = "Config Script";
            // 
            // mceSettings
            // 
            this.mceSettings.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.mceSettings.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::epubworkbench.Properties.Settings.Default, "MCESettings", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.mceSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mceSettings.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mceSettings.Location = new System.Drawing.Point(3, 48);
            this.mceSettings.Name = "mceSettings";
            this.mceSettings.Size = new System.Drawing.Size(432, 190);
            this.mceSettings.TabIndex = 1;
            this.mceSettings.Text = global::epubworkbench.Properties.Settings.Default.MCESettings;
            // 
            // lblWarning
            // 
            this.lblWarning.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblWarning.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblWarning.Image = global::epubworkbench.Properties.Resources.icn_warning;
            this.lblWarning.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblWarning.Location = new System.Drawing.Point(3, 16);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(432, 32);
            this.lblWarning.TabIndex = 0;
            this.lblWarning.Text = "Please do not modify config script this unless you\'ve tested it in a browser.";
            this.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // imgSettings
            // 
            this.imgSettings.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgSettings.ImageStream")));
            this.imgSettings.TransparentColor = System.Drawing.Color.Transparent;
            this.imgSettings.Images.SetKeyName(0, "wrench.png");
            this.imgSettings.Images.SetKeyName(1, "text-height.png");
            this.imgSettings.Images.SetKeyName(2, "embed.png");
            // 
            // frmSettings
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(482, 362);
            this.Controls.Add(this.tabSettings);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(498, 401);
            this.Name = "frmSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.frmSettings_Load);
            this.tabSettings.ResumeLayout(false);
            this.tabGeneral.ResumeLayout(false);
            this.tabGeneral.PerformLayout();
            this.tabTextEditor.ResumeLayout(false);
            this.tabTextEditor.PerformLayout();
            this.grpTextBehaviour.ResumeLayout(false);
            this.grpTextBehaviour.PerformLayout();
            this.grpTextDisplay.ResumeLayout(false);
            this.grpTextDisplay.PerformLayout();
            this.tabHTMLEditor.ResumeLayout(false);
            this.tabHTMLEditor.PerformLayout();
            this.grpMCESettings.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.TabControl tabSettings;
        private System.Windows.Forms.ImageList imgSettings;
        private System.Windows.Forms.TabPage tabTextEditor;
        private System.Windows.Forms.Button btnTextFont;
        private System.Windows.Forms.CheckBox chkTextShowLineNumbers;
        private System.Windows.Forms.FontDialog fnt;
        private System.Windows.Forms.GroupBox grpTextDisplay;
        private System.Windows.Forms.CheckBox chkTextConvertTabsToSpaces;
        private System.Windows.Forms.CheckBox chkTextWrapText;
        private System.Windows.Forms.TabPage tabHTMLEditor;
        private System.Windows.Forms.TabPage tabGeneral;
        private System.Windows.Forms.ComboBox cmbToolbarDisplay;
        private System.Windows.Forms.Label lblToolbarDisplay;
        private System.Windows.Forms.GroupBox grpMCESettings;
        private System.Windows.Forms.Label lblWarning;
        private System.Windows.Forms.RichTextBox mceSettings;
        private System.Windows.Forms.CheckBox chkTextEnabled;
        private System.Windows.Forms.GroupBox grpTextBehaviour;
        private System.Windows.Forms.CheckBox chkSaveOnChange;
        private System.Windows.Forms.CheckBox chkHTMLEnabled;
        private System.Windows.Forms.CheckBox chkSyntaxHighlighting;
        private System.Windows.Forms.ToolTip tipAbout;
    }
}