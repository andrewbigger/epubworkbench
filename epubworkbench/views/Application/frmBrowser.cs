﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using epubworkbench.models;
using epubworkbench.helpers;

namespace epubworkbench.views
{
    public partial class frmBrowser : Form
    {
        #region Instance Variables

        private DocumentCollection _files;
        private ToolStripItemDisplayStyle _style;

        #endregion

        #region Constructor and Loader

        public frmBrowser(DocumentCollection doc)
        {
            InitializeComponent();
            _files = doc;
        }

        private void frmBrowser_Load(object sender, EventArgs e)
        {
            SetDisplayStyle(Properties.Settings.Default.ToolbarDisplay);
            RefreshTree();
            tipAbout.SetToolTip(this.btnOpen, "Open file in workbench editor.");
            tipAbout.SetToolTip(this.btnDefault, "Open file in the system default application.");
            tipAbout.SetToolTip(this.btnExplorer, "Show file in Windows explorer.");
            tipAbout.SetToolTip(this.btnHideProperties, "Temporarily hide properties pane.");

        }

        private void frmBrowser_Close(object sender, FormClosingEventArgs e)
        {
            ApplicationHelper.Exit(e);
        }

        #endregion

        #region Event Handlers

        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RefreshDocumentCollection();
            RefreshTree();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenNode();
        }

        private void treeFiles_DoubleClick(object sender, EventArgs e)
        {
            OpenNode();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteNode();
            Reload();
        }

        private void refreshToolStripButton_Click(object sender, EventArgs e)
        {
            Reload();
        }

        private void deleteToolStripButton_Click(object sender, EventArgs e)
        {
            DeleteNode();
            Reload();
        }

        private void aboutToolStripItem_Click(object sender, EventArgs e)
        {
            ApplicationHelper.About();
        }

        private void documentationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ApplicationHelper.Documentation();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ApplicationHelper.Settings();
        }

        private void openToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            OpenNode();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            OpenNode();
        }

        private void btnDefault_Click(object sender, EventArgs e)
        {
            OpenNode("open-app");
        }

        private void openInSystemDefaultAppToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenNode("open-app");
        }

        private void btnExplorer_Click(object sender, EventArgs e)
        {
            OpenNode("open-explorer");
        }

        private void showInWindowsExplorerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenNode("open-explorer");
        }

        private void treeFiles_DoubleClick(object sender, TreeViewEventArgs e)
        {
            OpenNode();
        }

        private void treeFiles_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNode currentNode = this.treeFiles.SelectedNode;
            if (currentNode != null)
            {
                switch (currentNode.Tag.GetType().ToString())
                {
                    case "epubworkbench.models.Document":
                        Document doc = (Document)currentNode.Tag;
                        
                        if (ApplicationHelper.CanOpen(doc))
                        {
                            FocusActions("document");
                        }
                        else
                        {
                            FocusActions("unsupported-document");
                        }
                        break;
                    default:
                        FocusActions("documentcollection");
                        break;
                }

                PopulateProperties(currentNode);
            }
        }

        private void btnHideProperties_Click(object sender, EventArgs e)
        {
            this.pnlSplit.Panel2Collapsed = true;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ApplicationHelper.Exit();
        }


        #endregion

        #region Actions

        private void RefreshDocumentCollection()
        {
            _files.LoadCollection();
        }

        private void RefreshTree()
        {
            TreeNode docNode = BuildCollectionNode(_files);
            this.treeFiles.Nodes.Clear();
            this.treeFiles.Nodes.Add(docNode);
            docNode.Expand();
        }

        private void OpenNode()
        {
            OpenNode("open");
        }

        private void OpenNode(String action)
        {
            TreeNode currentNode = this.treeFiles.SelectedNode;

            if (currentNode != null && currentNode.Tag.GetType().ToString() == "epubworkbench.models.Document")
            {
                Document doc = (Document)currentNode.Tag;
                switch (action)
                {
                    case "open-app":
                        ApplicationHelper.OpenSystemDefault(doc);
                    break;
                    case "open-explorer":
                        ApplicationHelper.SelectExplorer(doc);
                    break;
                    default:
                    if (ApplicationHelper.CanOpen(doc) == true) { ApplicationHelper.OpenFile(doc); }
                    else { ApplicationHelper.OpenSystemDefault(doc); }
                    break;
                }

            }
            else if (currentNode != null && currentNode.Tag.GetType().ToString() == "epubworkbench.models.DocumentCollection")
            {
                DocumentCollection collection = (DocumentCollection)currentNode.Tag;
                switch (action)
                {
                    case "open-app":
                    case "open-explorer":
                        ApplicationHelper.SelectExplorer(collection);
                    break;
                    default:
                        // nothing
                    break;
                }
            }
            
        }

        private void DeleteNode()
        {
            TreeNode currentNode = this.treeFiles.SelectedNode;

            DialogResult deleteDecision = new DialogResult();
                deleteDecision = MessageBox.Show(
                "Are you sure you want to delete " + currentNode.Text + "?",
                "Confirm Delete",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);

            if (deleteDecision == DialogResult.Yes)
            {
                if (currentNode != null && currentNode.Tag.GetType().ToString() == "epubworkbench.models.Document")
                {
                    Document doc = (Document)currentNode.Tag;
                    doc.Delete();
                
                } 
                else if (currentNode.Tag.GetType().ToString() == "epubworkbench.models.DocumentCollection")
                {
                    DocumentCollection collection = (DocumentCollection)currentNode.Tag;
                    collection.Delete();
                }
            }
                
        }

        private void Reload()
        {
            RefreshDocumentCollection();
            RefreshTree();
        }

        private void FocusActions(String mode)
        {
            switch (mode)
            {
                case "document":
                    this.btnOpen.Enabled = true;
                    this.btnDefault.Enabled = true;
                    this.btnExplorer.Enabled = true;
                    break;
                case "unsupported-document":
                    this.btnOpen.Enabled = false;
                    this.btnDefault.Enabled = true;
                    this.btnExplorer.Enabled = true;
                    break;
                default:
                    this.btnOpen.Enabled = false;
                    this.btnDefault.Enabled = false;
                    this.btnExplorer.Enabled = true;

                    break;
            }
        }

        private void SetPreview()
        {
            
        }

        private void PopulateProperties(TreeNode currentNode)
        {
            if (currentNode != null)
            {
                switch (currentNode.Tag.GetType().ToString())
                {
                    case "epubworkbench.models.Document":
                        Document doc = (Document)currentNode.Tag;
                        this.lblName.Text = doc.Name;
                        if (doc.Type != "") { this.lblType.Text = "Type: " + doc.Type; } else { this.lblType.Text = ""; }
                        SetPreview(doc);  
                        break;
                    case "epubworkbench.models.DocumentCollection":
                        DocumentCollection collection = (DocumentCollection)currentNode.Tag;
                        this.lblName.Text = collection.Name;
                        this.lblType.Text = "Folder";
                        SetPreview(collection);
                        break;
                }

                this.pnlSplit.Panel2Collapsed = false;
            }
            else
            {
                this.lblName.Text = "";
            }
        }

        #endregion

        #region UI Builders

        private static TreeNode BuildCollectionNode(DocumentCollection collection)
        {
            TreeNode currentNode = new TreeNode();

            currentNode.ImageKey = "folder";
            currentNode.SelectedImageKey = "folder-open";
            currentNode.Text = collection.Name;
            currentNode.Tag = collection;

            foreach (Object item in collection.Contents)
            {
                switch (item.GetType().ToString())
                {
                    case "epubworkbench.models.DocumentCollection":
                        currentNode.Nodes.Add(BuildCollectionNode((DocumentCollection)item));
                        break;

                    default:
                        currentNode.Nodes.Add(BuildDocumentNode((Document)item));
                        break;
                }
                
            }

            return currentNode;
        }

        private static TreeNode BuildDocumentNode(Document doc)
        {
            TreeNode currentNode = new TreeNode();

            currentNode.ImageKey = GetIcon(doc.Type);
            currentNode.SelectedImageKey = GetIcon(doc.Type);
            currentNode.Text = doc.Name;
            currentNode.Tag = doc;

            return currentNode;
        }

        private static TreeNode AddChildren(TreeNode parent, TreeNode child)
        {
            parent.Nodes.Add(child);
            return parent;
        }

        private void SetDisplayStyle(String styleSetting)
        {
            switch (styleSetting)
            {
                case "Icon Only":
                    _style = ToolStripItemDisplayStyle.Image;
                    break;
                case "Icon and Text":
                    _style = ToolStripItemDisplayStyle.ImageAndText;
                    break;
                default:
                    _style = ToolStripItemDisplayStyle.Text;
                    break;
            }
            foreach (Object item in this.tolActions.Items)
            {
                try
                {
                    ToolStripButton button = (ToolStripButton)item;
                    button.DisplayStyle = _style;
                }
                catch (InvalidCastException)
                {
                    //ignore
                }
            }
        }

        private static String GetIcon(String fileType)
        {
            String key = "";

            switch (fileType)
            {
                case ".html":
                case ".xhtml":
                    key = "file-xml";
                    break;
                case ".css":
                    key = "file-css";
                    break;
                case ".ttf":
                    key = "font";
                    break;
                case ".plist":
                case ".opf":
                case ".ncx":
                case ".xml":
                    key = "file-known";
                    break;
                case ".jpg":
                case ".png":
                case ".tiff":
                case ".bmp": 
                case ".jpeg":
                case ".gif":
                    key = "image";
                    break;
                default:
                    key = "file-unknown";
                    break;

            }

            return key;
        }


        #endregion

        private void lblName_Click(object sender, EventArgs e)
        {

        }

        private void SetPreview(DocumentCollection coll)
        {
            this.grpPreview.Visible = false;
        }

        private void SetPreview(Document doc)
        {
            if (ApplicationHelper.CanPreview(doc) == false)
            {
                this.grpPreview.Visible = false;
            }
            else
            {
                this.previewBrowser.AllowNavigation = true;
                this.previewBrowser.Navigate(doc.Location);
            }

        }

        private void Preview_Finished_Nav(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            try
            {
                this.previewBrowser.Document.Body.Style = "zoom: 20%";
                this.grpPreview.Visible = true;
                this.previewBrowser.AllowNavigation = false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.grpPreview.Visible = false;
            }
        }


    }
}
