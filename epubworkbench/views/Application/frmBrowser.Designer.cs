﻿namespace epubworkbench.views
{
    partial class frmBrowser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBrowser));
            this.icons = new System.Windows.Forms.ImageList(this.components);
            this.mnuContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openInSystemDefaultAppToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showInWindowsExplorerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tolContainer = new System.Windows.Forms.ToolStripContainer();
            this.pnlSplit = new System.Windows.Forms.SplitContainer();
            this.treeFiles = new System.Windows.Forms.TreeView();
            this.pnlProperties = new System.Windows.Forms.Panel();
            this.grpPreview = new System.Windows.Forms.GroupBox();
            this.previewBrowser = new System.Windows.Forms.WebBrowser();
            this.blankContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnHideProperties = new System.Windows.Forms.Button();
            this.lblType = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.grpActions = new System.Windows.Forms.GroupBox();
            this.btnExplorer = new System.Windows.Forms.Button();
            this.btnDefault = new System.Windows.Forms.Button();
            this.btnOpen = new System.Windows.Forms.Button();
            this.tolActions = new System.Windows.Forms.ToolStrip();
            this.PrefsButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.aboutToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.documentationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.drawerSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.refreshToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.deleteToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.tipAbout = new System.Windows.Forms.ToolTip(this.components);
            this.mnuContext.SuspendLayout();
            this.tolContainer.ContentPanel.SuspendLayout();
            this.tolContainer.TopToolStripPanel.SuspendLayout();
            this.tolContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSplit)).BeginInit();
            this.pnlSplit.Panel1.SuspendLayout();
            this.pnlSplit.Panel2.SuspendLayout();
            this.pnlSplit.SuspendLayout();
            this.pnlProperties.SuspendLayout();
            this.grpPreview.SuspendLayout();
            this.grpActions.SuspendLayout();
            this.tolActions.SuspendLayout();
            this.SuspendLayout();
            // 
            // icons
            // 
            this.icons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("icons.ImageStream")));
            this.icons.TransparentColor = System.Drawing.Color.Transparent;
            this.icons.Images.SetKeyName(0, "folder");
            this.icons.Images.SetKeyName(1, "file-unknown");
            this.icons.Images.SetKeyName(2, "file-known");
            this.icons.Images.SetKeyName(3, "file-css");
            this.icons.Images.SetKeyName(4, "file-xml");
            this.icons.Images.SetKeyName(5, "image");
            this.icons.Images.SetKeyName(6, "folder-open");
            this.icons.Images.SetKeyName(7, "font");
            this.icons.Images.SetKeyName(8, "list");
            // 
            // mnuContext
            // 
            this.mnuContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshToolStripMenuItem,
            this.toolStripSeparator1,
            this.deleteToolStripMenuItem,
            this.toolStripSeparator2,
            this.openToolStripMenuItem,
            this.openInSystemDefaultAppToolStripMenuItem,
            this.showInWindowsExplorerToolStripMenuItem});
            this.mnuContext.Name = "mnuContext";
            this.mnuContext.Size = new System.Drawing.Size(262, 126);
            // 
            // refreshToolStripMenuItem
            // 
            this.refreshToolStripMenuItem.Image = global::epubworkbench.Properties.Resources.icn_refresh;
            this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
            this.refreshToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.refreshToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.refreshToolStripMenuItem.Text = "&Refresh";
            this.refreshToolStripMenuItem.Click += new System.EventHandler(this.refreshToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(258, 6);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Image = global::epubworkbench.Properties.Resources.icn_close;
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.deleteToolStripMenuItem.Text = "&Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(258, 6);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Image = global::epubworkbench.Properties.Resources.icn_open;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click_1);
            // 
            // openInSystemDefaultAppToolStripMenuItem
            // 
            this.openInSystemDefaultAppToolStripMenuItem.Image = global::epubworkbench.Properties.Resources.icn_open_default;
            this.openInSystemDefaultAppToolStripMenuItem.Name = "openInSystemDefaultAppToolStripMenuItem";
            this.openInSystemDefaultAppToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D)));
            this.openInSystemDefaultAppToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.openInSystemDefaultAppToolStripMenuItem.Text = "Open in System Default App";
            this.openInSystemDefaultAppToolStripMenuItem.Click += new System.EventHandler(this.openInSystemDefaultAppToolStripMenuItem_Click);
            // 
            // showInWindowsExplorerToolStripMenuItem
            // 
            this.showInWindowsExplorerToolStripMenuItem.Image = global::epubworkbench.Properties.Resources.icn_open_windows;
            this.showInWindowsExplorerToolStripMenuItem.Name = "showInWindowsExplorerToolStripMenuItem";
            this.showInWindowsExplorerToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.S)));
            this.showInWindowsExplorerToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.showInWindowsExplorerToolStripMenuItem.Text = "Show in Windows Explorer";
            this.showInWindowsExplorerToolStripMenuItem.Click += new System.EventHandler(this.showInWindowsExplorerToolStripMenuItem_Click);
            // 
            // tolContainer
            // 
            // 
            // tolContainer.ContentPanel
            // 
            this.tolContainer.ContentPanel.AutoScroll = true;
            this.tolContainer.ContentPanel.Controls.Add(this.pnlSplit);
            this.tolContainer.ContentPanel.Size = new System.Drawing.Size(479, 405);
            this.tolContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tolContainer.Location = new System.Drawing.Point(0, 0);
            this.tolContainer.Name = "tolContainer";
            this.tolContainer.Size = new System.Drawing.Size(479, 430);
            this.tolContainer.TabIndex = 2;
            this.tolContainer.Text = "toolStripContainer1";
            // 
            // tolContainer.TopToolStripPanel
            // 
            this.tolContainer.TopToolStripPanel.Controls.Add(this.tolActions);
            // 
            // pnlSplit
            // 
            this.pnlSplit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSplit.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.pnlSplit.Location = new System.Drawing.Point(0, 0);
            this.pnlSplit.Name = "pnlSplit";
            // 
            // pnlSplit.Panel1
            // 
            this.pnlSplit.Panel1.Controls.Add(this.treeFiles);
            // 
            // pnlSplit.Panel2
            // 
            this.pnlSplit.Panel2.Controls.Add(this.pnlProperties);
            this.pnlSplit.Panel2MinSize = 197;
            this.pnlSplit.Size = new System.Drawing.Size(479, 405);
            this.pnlSplit.SplitterDistance = 249;
            this.pnlSplit.TabIndex = 0;
            // 
            // treeFiles
            // 
            this.treeFiles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeFiles.ImageIndex = 0;
            this.treeFiles.ImageList = this.icons;
            this.treeFiles.Location = new System.Drawing.Point(0, 0);
            this.treeFiles.Name = "treeFiles";
            this.treeFiles.SelectedImageIndex = 0;
            this.treeFiles.Size = new System.Drawing.Size(249, 405);
            this.treeFiles.TabIndex = 2;
            this.treeFiles.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeFiles_AfterSelect);
            this.treeFiles.DoubleClick += new System.EventHandler(this.treeFiles_DoubleClick);
            // 
            // pnlProperties
            // 
            this.pnlProperties.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlProperties.Controls.Add(this.grpPreview);
            this.pnlProperties.Controls.Add(this.btnHideProperties);
            this.pnlProperties.Controls.Add(this.lblType);
            this.pnlProperties.Controls.Add(this.lblName);
            this.pnlProperties.Controls.Add(this.grpActions);
            this.pnlProperties.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlProperties.Location = new System.Drawing.Point(0, 0);
            this.pnlProperties.Name = "pnlProperties";
            this.pnlProperties.Size = new System.Drawing.Size(226, 405);
            this.pnlProperties.TabIndex = 0;
            // 
            // grpPreview
            // 
            this.grpPreview.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpPreview.Controls.Add(this.previewBrowser);
            this.grpPreview.Location = new System.Drawing.Point(13, 55);
            this.grpPreview.Name = "grpPreview";
            this.grpPreview.Size = new System.Drawing.Size(199, 191);
            this.grpPreview.TabIndex = 4;
            this.grpPreview.TabStop = false;
            this.grpPreview.Text = "Preview";
            this.tipAbout.SetToolTip(this.grpPreview, "Document preview.");
            // 
            // previewBrowser
            // 
            this.previewBrowser.AllowWebBrowserDrop = false;
            this.previewBrowser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.previewBrowser.ContextMenuStrip = this.blankContextMenu;
            this.previewBrowser.IsWebBrowserContextMenuEnabled = false;
            this.previewBrowser.Location = new System.Drawing.Point(35, 16);
            this.previewBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.previewBrowser.Name = "previewBrowser";
            this.previewBrowser.ScriptErrorsSuppressed = true;
            this.previewBrowser.ScrollBarsEnabled = false;
            this.previewBrowser.Size = new System.Drawing.Size(133, 172);
            this.previewBrowser.TabIndex = 0;
            this.previewBrowser.WebBrowserShortcutsEnabled = false;
            this.previewBrowser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.Preview_Finished_Nav);
            // 
            // blankContextMenu
            // 
            this.blankContextMenu.Name = "blankContextMenu";
            this.blankContextMenu.Size = new System.Drawing.Size(61, 4);
            // 
            // btnHideProperties
            // 
            this.btnHideProperties.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHideProperties.Location = new System.Drawing.Point(13, 368);
            this.btnHideProperties.Name = "btnHideProperties";
            this.btnHideProperties.Size = new System.Drawing.Size(199, 23);
            this.btnHideProperties.TabIndex = 3;
            this.btnHideProperties.Text = "Hide";
            this.btnHideProperties.UseVisualStyleBackColor = true;
            this.btnHideProperties.Click += new System.EventHandler(this.btnHideProperties_Click);
            // 
            // lblType
            // 
            this.lblType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblType.Location = new System.Drawing.Point(14, 35);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(198, 17);
            this.lblType.TabIndex = 2;
            this.lblType.Text = "type";
            // 
            // lblName
            // 
            this.lblName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(13, 13);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(199, 20);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "File Name";
            this.lblName.Click += new System.EventHandler(this.lblName_Click);
            // 
            // grpActions
            // 
            this.grpActions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpActions.Controls.Add(this.btnExplorer);
            this.grpActions.Controls.Add(this.btnDefault);
            this.grpActions.Controls.Add(this.btnOpen);
            this.grpActions.Location = new System.Drawing.Point(13, 252);
            this.grpActions.Name = "grpActions";
            this.grpActions.Size = new System.Drawing.Size(199, 108);
            this.grpActions.TabIndex = 0;
            this.grpActions.TabStop = false;
            this.grpActions.Text = "Actions";
            // 
            // btnExplorer
            // 
            this.btnExplorer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExplorer.Image = global::epubworkbench.Properties.Resources.icn_open_windows;
            this.btnExplorer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExplorer.Location = new System.Drawing.Point(6, 77);
            this.btnExplorer.Name = "btnExplorer";
            this.btnExplorer.Size = new System.Drawing.Size(189, 23);
            this.btnExplorer.TabIndex = 2;
            this.btnExplorer.Text = "Show in &Explorer";
            this.btnExplorer.UseVisualStyleBackColor = true;
            this.btnExplorer.Click += new System.EventHandler(this.btnExplorer_Click);
            // 
            // btnDefault
            // 
            this.btnDefault.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDefault.Enabled = false;
            this.btnDefault.Image = global::epubworkbench.Properties.Resources.icn_open_default;
            this.btnDefault.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDefault.Location = new System.Drawing.Point(6, 48);
            this.btnDefault.Name = "btnDefault";
            this.btnDefault.Size = new System.Drawing.Size(189, 23);
            this.btnDefault.TabIndex = 1;
            this.btnDefault.Text = "Open with &Default App";
            this.btnDefault.UseVisualStyleBackColor = true;
            this.btnDefault.Click += new System.EventHandler(this.btnDefault_Click);
            // 
            // btnOpen
            // 
            this.btnOpen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpen.Enabled = false;
            this.btnOpen.Image = global::epubworkbench.Properties.Resources.icn_open;
            this.btnOpen.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOpen.Location = new System.Drawing.Point(6, 19);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(189, 23);
            this.btnOpen.TabIndex = 0;
            this.btnOpen.Text = "&Open";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // tolActions
            // 
            this.tolActions.Dock = System.Windows.Forms.DockStyle.None;
            this.tolActions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PrefsButton,
            this.drawerSeparator,
            this.refreshToolStripButton,
            this.deleteToolStripButton});
            this.tolActions.Location = new System.Drawing.Point(3, 0);
            this.tolActions.Name = "tolActions";
            this.tolActions.Size = new System.Drawing.Size(93, 25);
            this.tolActions.TabIndex = 0;
            // 
            // PrefsButton
            // 
            this.PrefsButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.PrefsButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripItem,
            this.documentationToolStripMenuItem,
            this.toolStripSeparator3,
            this.settingsToolStripMenuItem,
            this.toolStripSeparator4,
            this.exitToolStripMenuItem});
            this.PrefsButton.Image = global::epubworkbench.Properties.Resources.icn_drawer;
            this.PrefsButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PrefsButton.Name = "PrefsButton";
            this.PrefsButton.Size = new System.Drawing.Size(29, 22);
            this.PrefsButton.Text = "About";
            this.PrefsButton.ToolTipText = "Toolbar function drawer. \r\n\r\nClick to reveal dropdown menu.";
            // 
            // aboutToolStripItem
            // 
            this.aboutToolStripItem.Image = global::epubworkbench.Properties.Resources.icn_about;
            this.aboutToolStripItem.Name = "aboutToolStripItem";
            this.aboutToolStripItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.aboutToolStripItem.Size = new System.Drawing.Size(176, 22);
            this.aboutToolStripItem.Text = "About";
            this.aboutToolStripItem.ToolTipText = "About ePub Workbench.";
            this.aboutToolStripItem.Click += new System.EventHandler(this.aboutToolStripItem_Click);
            // 
            // documentationToolStripMenuItem
            // 
            this.documentationToolStripMenuItem.Image = global::epubworkbench.Properties.Resources.icn_documentation;
            this.documentationToolStripMenuItem.Name = "documentationToolStripMenuItem";
            this.documentationToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.documentationToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.documentationToolStripMenuItem.Text = "Documentation";
            this.documentationToolStripMenuItem.ToolTipText = "Go to Workbench documentation.";
            this.documentationToolStripMenuItem.Click += new System.EventHandler(this.documentationToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(173, 6);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Image = global::epubworkbench.Properties.Resources.icn_settings;
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.ShortcutKeyDisplayString = "Ctl+,";
            this.settingsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Oemcomma)));
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.ToolTipText = "Modify Workbench settings.";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(173, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Image = global::epubworkbench.Properties.Resources.icn_exit;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.ToolTipText = "Close all files and exit the Application";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // drawerSeparator
            // 
            this.drawerSeparator.Name = "drawerSeparator";
            this.drawerSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // refreshToolStripButton
            // 
            this.refreshToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.refreshToolStripButton.Image = global::epubworkbench.Properties.Resources.icn_refresh;
            this.refreshToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.refreshToolStripButton.Name = "refreshToolStripButton";
            this.refreshToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.refreshToolStripButton.Text = "Refresh";
            this.refreshToolStripButton.ToolTipText = "Reload all files from disk into tree pane.";
            this.refreshToolStripButton.Click += new System.EventHandler(this.refreshToolStripButton_Click);
            // 
            // deleteToolStripButton
            // 
            this.deleteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.deleteToolStripButton.Image = global::epubworkbench.Properties.Resources.icn_close;
            this.deleteToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deleteToolStripButton.Name = "deleteToolStripButton";
            this.deleteToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.deleteToolStripButton.Text = "Delete";
            this.deleteToolStripButton.ToolTipText = "Delete selected file.";
            this.deleteToolStripButton.Click += new System.EventHandler(this.deleteToolStripButton_Click);
            // 
            // frmBrowser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(479, 430);
            this.ContextMenuStrip = this.mnuContext;
            this.Controls.Add(this.tolContainer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(348, 385);
            this.Name = "frmBrowser";
            this.Text = "Browser";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmBrowser_Close);
            this.Load += new System.EventHandler(this.frmBrowser_Load);
            this.mnuContext.ResumeLayout(false);
            this.tolContainer.ContentPanel.ResumeLayout(false);
            this.tolContainer.TopToolStripPanel.ResumeLayout(false);
            this.tolContainer.TopToolStripPanel.PerformLayout();
            this.tolContainer.ResumeLayout(false);
            this.tolContainer.PerformLayout();
            this.pnlSplit.Panel1.ResumeLayout(false);
            this.pnlSplit.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlSplit)).EndInit();
            this.pnlSplit.ResumeLayout(false);
            this.pnlProperties.ResumeLayout(false);
            this.grpPreview.ResumeLayout(false);
            this.grpActions.ResumeLayout(false);
            this.tolActions.ResumeLayout(false);
            this.tolActions.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip mnuContext;
        private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem;
        private System.Windows.Forms.ImageList icons;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripContainer tolContainer;
        private System.Windows.Forms.ToolStrip tolActions;
        private System.Windows.Forms.ToolStripButton refreshToolStripButton;
        private System.Windows.Forms.ToolStripButton deleteToolStripButton;
        private System.Windows.Forms.ToolStripDropDownButton PrefsButton;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem documentationToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator drawerSeparator;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.SplitContainer pnlSplit;
        private System.Windows.Forms.TreeView treeFiles;
        private System.Windows.Forms.Panel pnlProperties;
        private System.Windows.Forms.GroupBox grpActions;
        private System.Windows.Forms.Button btnExplorer;
        private System.Windows.Forms.Button btnDefault;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Button btnHideProperties;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem openInSystemDefaultAppToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showInWindowsExplorerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.GroupBox grpPreview;
        private System.Windows.Forms.WebBrowser previewBrowser;
        private System.Windows.Forms.ContextMenuStrip blankContextMenu;
        private System.Windows.Forms.ToolTip tipAbout;

    }
}