﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using epubworkbench.models;
using epubworkbench.helpers;
using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Highlighting;
using ICSharpCode.AvalonEdit.Highlighting.Xshd;
using System.Xml;
using System.Windows.Forms.Integration;


namespace epubworkbench.views
{
    public partial class frmText : Form
    {

        #region Instance Variables

        private Document _file;
        private TextEditor _edit = new TextEditor();
        private ToolStripItemDisplayStyle _style;

        #endregion

        #region Constructor and Loader

        public frmText(Document doc)
        {
            InitializeComponent();
            _file = doc;
            this.Text = doc.Name;
        }

        private void frmText_Load(object sender, EventArgs e)
        {
            SetTextArea();
            SetDisplayStyle(Properties.Settings.Default.ToolbarDisplay);
            Reload();
        }

        #endregion

        #region Event Handlers

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void saveToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Save();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Reload();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseWindow();
        }

        private void closeToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            CloseWindow();
        }

        private void tolSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void cutToolStripButton_Click(object sender, EventArgs e)
        {
            SendKeys.Send("^x");
        }

        private void copyToolStripButton_Click(object sender, EventArgs e)
        {
            SendKeys.Send("^c");
        }

        private void pasteToolStripButton_Click(object sender, EventArgs e)
        {
            SendKeys.Send("^v");
        }

        private void refreshToolStripButton_Click(object sender, EventArgs e)
        {
            Reload();
        }

        private void openInSystemDefaultAppToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ApplicationHelper.OpenSystemDefault(_file);
        }

        private void showInWindowsExplorerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ApplicationHelper.SelectExplorer(_file);
        }

        #endregion

        #region Actions

        private void Reload()
        {
            _edit.Text = _file.Read;
        }
        
        private void Save()
        {
            _file.Write(_edit.Text);
        }

        private void CloseWindow()
        {
            this.Close();
        }

        private void SetTextArea()
        {
            ElementHost host = new ElementHost();
            host.Dock = DockStyle.Fill;

            _edit.ShowLineNumbers = Properties.Settings.Default.TextWindowShowNumbers;
            _edit.WordWrap = Properties.Settings.Default.TextWindowWrapText;
            _edit.FontFamily = new System.Windows.Media.FontFamily("Consolas");
            _edit.FontSize = 12.75f;

            host.Child = _edit;

            this.pnlEditor.Controls.Add(host);
        }

        private void SetDisplayStyle(String styleSetting)
        {
            switch (styleSetting)
            {
                case "Icon Only":
                    _style = ToolStripItemDisplayStyle.Image;
                    break;
                case "Icon and Text":
                    _style = ToolStripItemDisplayStyle.ImageAndText;
                    break;
                default:
                    _style = ToolStripItemDisplayStyle.Text;
                    break;
            }
            foreach (Object b in this.tolActions.Items)
            {
                try
                {
                    ToolStripButton btn = (ToolStripButton)b;
                    btn.DisplayStyle = _style;

                }
                catch (InvalidCastException)
                {
                    //ignore
                }
            }

        }

        #endregion

    }
}
