﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using epubworkbench.models;
using epubworkbench.helpers;
using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Highlighting;
using ICSharpCode.AvalonEdit.Highlighting.Xshd;
using System.Xml;
using System.Windows.Forms.Integration;


namespace epubworkbench.views
{
    public partial class frmHtml : Form
    {

        #region Instance Variables

        private XhtmlDocument _file;
        private WebBrowser _view = new WebBrowser();
        private ToolStripItemDisplayStyle _style;
        private Boolean _allowEdit;

        #endregion

        #region Constructor and Loader

        public frmHtml(XhtmlDocument doc, Boolean allowEdit)
        {
            InitializeComponent();
            _file = doc;
            _allowEdit = allowEdit;
            this.Text = doc.Name;
        }

        private void frmText_Load(object sender, EventArgs e)
        {
            SetBrowser();
            SetDisplayStyle(Properties.Settings.Default.ToolbarDisplay);
            SetMode();
            Reload();
        }

        private void frmText_FormClosing(object sender, FormClosingEventArgs e)
        {
            _file.Clean();
        }

        #endregion

        #region Event Handlers

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void saveToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Save();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Reload();
        }

        private void closeToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            CloseWindow();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseWindow();
        }

        private void tolSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void cutToolStripButton_Click(object sender, EventArgs e)
        {
            SendKeys.Send("^x");
        }

        private void copyToolStripButton_Click(object sender, EventArgs e)
        {
            SendKeys.Send("^c");
        }

        private void pasteToolStripButton_Click(object sender, EventArgs e)
        {
            SendKeys.Send("^v");
        }

        private void refreshToolStripButton_Click(object sender, EventArgs e)
        {
            Reload();
        }

        private void openInSystemDefaultAppToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ApplicationHelper.OpenSystemDefault(_file);
        }

        private void showInWindowsExplorerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ApplicationHelper.SelectExplorer(_file);
        }

        private void viewSourceToolStripButton_Click(object sender, EventArgs e)
        {
            ApplicationHelper.OpenFile(_file, new frmText(_file));
        }

        #endregion

        #region Actions

        private void Reload()
        {
            _view.Refresh();
        }
        
        private void Save()
        {
            _file.Write(_view.Document.GetElementsByTagName("html")[0].OuterHtml);
            _file.Clean();
            _file.MakeEditable();
        }

        private void CloseWindow()
        {
            this.Close();
        }

        private void SetBrowser()
        {
            _view.Dock = DockStyle.Fill;
            _view.IsWebBrowserContextMenuEnabled = false;
            _file.MakeEditable();
            _view.Navigate(_file.Location);
            
            this.pnlEditor.Controls.Add(_view);
        }

        private void SetDisplayStyle(String styleSetting)
        {
            switch (styleSetting)
            {
                case "Icon Only":
                    _style = ToolStripItemDisplayStyle.Image;
                    break;
                case "Icon and Text":
                    _style = ToolStripItemDisplayStyle.ImageAndText;
                    break;
                default:
                    _style = ToolStripItemDisplayStyle.Text;
                    break;
            }
            foreach (Object b in this.tolActions.Items)
            {
                try
                {
                    ToolStripButton btn = (ToolStripButton)b;
                    btn.DisplayStyle = _style;

                }
                catch (InvalidCastException)
                {
                    //ignore
                }
            }

        }

        private void SetMode()
        {
            this.saveToolStripButton.Enabled = _allowEdit;
            this.cutToolStripButton.Enabled = _allowEdit;
            this.pasteToolStripButton.Enabled = _allowEdit;
            this.saveToolStripMenuItem.Enabled = _allowEdit;
        }


        #endregion

    }
}
