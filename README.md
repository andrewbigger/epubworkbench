## Getting Started ##

The ePub Workbench is a collection of tools for editing expanded ePubs, and validating ePub files. This tool will only work on expanded ePub files, if you are unsure as to how to do that, then please see this [article](http://www.ehow.com/how_12158729_extract-epub-files.html). 

Before you get started you'll need to ensure that you have the following:

* Windows 7 or later
* Microsoft .NET Framework 4.5
* A tool for expanding ePub files.

### What is this repository for? ###

* This is the application and test repository of the work bench application.
* 1.0

### How do I get set up? ###

The ePub Workbench is a .NET application written in C#. So before you begin ensure you have the following installed:

* Microsoft Visual Studio 2012 or later
* MS.NET Framework 4.5

All other dependencies should be available in VisualStudio.

You can run application tests from the test project in VisualStudio.

### Contribution guidelines ###

I'm not accepting contributions at this point in time.

### Who do I talk to? ###